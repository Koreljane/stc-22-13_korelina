package homework2;

public class HomeWorkMove {
    static void moveNumbers(int[] array, int n) {
        int zero = 0;
        for (int i = 0; i < n; i++)
            if (array [i] != 0)
                array[zero++] = array[i];
        for (int i = zero; i<n; i++)
            array[i]=0;
    }
    static void printArray(int[] array, int n) {
        for (int i = 0; i < n ; i++)
            System.out.print(array[i] + " ");
    }

    public static void main(String[] args) {
        int[] array = {2, 0, 7, 0, 5, 0, 0, 0, 16, -1, 5, 0, 9};
        int n = array.length;

        System.out.print("Original: ");
        printArray(array, n);

        moveNumbers(array, n);

        System.out.print("Modified: ");
        printArray(array, n);
    }
}