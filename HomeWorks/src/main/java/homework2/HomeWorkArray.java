package homework2;

public class HomeWorkArray {
    public static int getPosition(int[] array, int x) {

        for (int i = 0; i < array.length ; i++) {
            if (array[i] == x) return i;
        }
        return -1;
    }

    public static void main(String[] args) {
        int [] a = {3,10,-5,15,23,48,-30};
        int x = 15;
        System.out.println(getPosition(a, x));
    }
}
